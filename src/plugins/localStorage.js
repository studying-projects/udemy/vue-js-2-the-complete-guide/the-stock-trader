class LocalStorageWrapper {

	get(key, ifEmpty = []) {
		const data = localStorage.getItem(key);

		if (!data) {
			return ifEmpty;
		}

		return JSON.parse(data);
	}

	set(key, value) {
		localStorage.setItem(key, JSON.stringify(value));
	}

}

const LocalStorage = {
	install(Vue) {
		Vue.prototype.$ls = new LocalStorageWrapper();
	},
};

export default LocalStorage;
