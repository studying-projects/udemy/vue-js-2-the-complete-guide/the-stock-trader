import * as finnhub from 'finnhub';
		
const api_key = finnhub.ApiClient.instance.authentications['api_key'];
api_key.apiKey = process.env.VUE_APP_FINNHUB_API_KEY;

export default new finnhub.DefaultApi();