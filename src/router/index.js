import Vue from 'vue';
import VueRouter from 'vue-router';
import firebase from 'firebase';

import Home from '../views/Home';
import About from '../views/About';
import Portfolio from '../views/Portfolio';
import Stocks from '../views/Stocks';
import Account from '../views/Account';
import NotFound from '../views/NotFound';
import Login from '../views/Login';
import Stock from '../components/stocks/Stock';

Vue.use(VueRouter);

const routes = [
	{ path: '/', component: Home, name: 'home' },
	{ path: '/about', component: About, name: 'about' },
	{ path: '/portfolio', component: Portfolio, name: 'portfolio', meta: { requiresAuth: true } },
	{ path: '/portfolio/:id', component: Stock, meta: { requiresAuth: true } },
	{ path: '/stocks', component: Stocks, name: 'stocks' },
	{ path: '/stocks/:id', component: Stock, name: 'stock' },
	{ path: '/account', component: Account, name: 'account', meta: { requiresAuth: true } },
	{ path: '/login', component: Login, name: 'login' },
	{ path: '*', component: NotFound },
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
	const currentUser = firebase.auth().currentUser;

	if (requiresAuth && !currentUser) {
		next('/login');
	}
	else {
		next();
	}
})

export default router;
