import Vue from 'vue';
import Vuex from 'vuex';
import fb from '../util/firebase';

import auth from './modules/auth';
import stocks from './modules/stocks';
import watchlist from './modules/watchlist';
import portfolio from './modules/portfolio';
import account from './modules/account';

Vue.use(Vuex);

fb.auth.onAuthStateChanged(user => {
	if (user) {
		store.commit('setCurrentUser', user);
		store.dispatch('fetchUserProfile');

		fb.usersCollection.doc(user.uid).onSnapshot(doc => {
			store.commit('SET_USER_PROFILE', doc.data());
		});
	}
});

export default new Vuex.Store({
	modules: {
		auth,
		stocks,
		watchlist,
		portfolio,
		account,
	}
});
