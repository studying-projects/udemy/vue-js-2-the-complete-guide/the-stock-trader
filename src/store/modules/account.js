const state = {
	firstName: 'Jack',
	lastName: 'Package',
	email: 'jack.package@example.com',
	balance: 10000,
};

const getters = {
	fullName(state) {
		return state.firstName + ' ' + state.lastName;
	},
	balance(state) {
		return state.balance;
	},
};

const mutations = {
	
};

const actions = {
	
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters,
};