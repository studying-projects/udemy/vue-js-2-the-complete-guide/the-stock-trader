import finnhubClient from '@/plugins/finnhubClient';
import eventBus from '@/util/eventBus';
import { Stocks } from '@/models/Stock';

const state = {
	allAvailableStocks: [],
	stocks: new Stocks(),
};

const getters = {
	allAvailableStocks: state => state.allAvailableStocks,
	stocks: state => state.stocks,
};

const mutations = {
	SET_ALL_AWAILABLE_STOCKS(state, stocks) {
		state.allAvailableStocks = stocks;
	},
};

const actions = {

	// Load all stock names and tickets for US
	loadAllAvailableStocks: ({ commit }) => {
		
		eventBus.$emit('loadingAllAvailableStocks', true);

		const allAwailableStocksTimastamp = localStorage.getItem('all-available-stocks-timestamp');
		const seconsIn30days = 60 * 60 * 24 * 30; // 30 days
		const isExpired = allAwailableStocksTimastamp < (new Date().getTime() - seconsIn30days);
		
		if (!isExpired) {
			// Check localStorage first, set to storage
			let localStorageStocks = localStorage.getItem('all-available-stocks');
			if (!!localStorageStocks) {
				localStorageStocks = JSON.parse(localStorageStocks);
			}
			
			commit('SET_ALL_AWAILABLE_STOCKS', localStorageStocks);
			
			eventBus.$emit('loadingAllAvailableStocks', false);
			return;
		}	

		// If no data in localStorage, or it is older than 30 days, fetch
		finnhubClient.stockSymbols('US', (e, data, r) => {

			/** @todo show error */
			if (e !== null) {
				console.log(e);
				eventBus.$emit('loadingAllAvailableStocks', false);
			}

			localStorage.setItem('all-available-stocks-timestamp', new Date().getTime());
			localStorage.setItem('all-available-stocks', JSON.stringify(data));

			commit('SET_ALL_AWAILABLE_STOCKS', data);

			eventBus.$emit('loadingAllAvailableStocks', false);
		});
	},

	
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
};