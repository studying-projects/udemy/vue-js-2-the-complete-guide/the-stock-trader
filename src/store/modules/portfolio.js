const state = {
	stocks: []
};
const getters = {

	stocks: state => state.stocks,

};

const mutations = {

	SET_STOCKS(state, stocks) {
		state.stocks = stocks;
	}

};

const actions = {

	loadStocks({ commit }) {

	}

};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
};
