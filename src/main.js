import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';

import vuetify from './plugins/vuetify';
import localStorage from './plugins/localStorage';

import fb from './util/firebase';

Vue.config.productionTip = false;

Vue.filter('money', (value) => {
	return '$' + value.toLocaleString();
});

// axios.defaults.baseURL = 'https://vue-update.firebaseio.com';
// axios.defaults.headers.common['Authorization'] = 'fasfdsa'
// axios.defaults.headers.get['Accepts'] = 'application/json';

Vue.use(localStorage);

let app;
fb.auth.onAuthStateChanged(user => {
	if (!app) {
		app = new Vue({
			router,
			store,
			vuetify,
			render: h => h(App)
		}).$mount('#app');
	}
});
