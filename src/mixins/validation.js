export default {
	data() {
		return {
			validate: {
				require: [
					v => !!v || 'Field is required',
					v => (!!v && v.trim().length > 0) || 'Cannot be only whitespace',
				],
				email: (v) => {
					const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					return pattern.test(v) || 'Invalid email';
				},
				checkConfirmPassword(password, confirmPassword) {
					if (password === confirmPassword)
						return true;
		
					return 'Passwords do not match';
				},
			}
		};
	},
	methods: {
		
	},
};