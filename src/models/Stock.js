import finnhubClient from '@/plugins/finnhubClient';
// import store from '@/store';

/**
 * Stock model
 */
export class Stock {

	ticker = '';
	exchange = '';
	finnhubIndustry = '';
	logo = '';
	marketCapitalization = 0;
	name = '';
	shareOutstanding = 0;
	weburl = '';

	price;
	prices = [];
	loading = false;

	/**
	 * 
	 * @param {Object} data
	 */
	constructor(ticker) {
		if (typeof ticker !== 'string') {
			throw new Error('Wrong argument in constructor.');
		}
		this.ticker = ticker.toUpperCase();
	}

	fetch() {
		return new Promise((resolve, reject) => {
			
			if (this.ticker.length === 0) {
				reject('Ticker is empty');
			}

			this.loading = true;
			
			finnhubClient.companyProfile2({ symbol: this.ticker }, (e, companyProfile, r) => {
				
				if (e !== null) {
					this.loading = false;
					reject({ error: e, response: r });
				}
				
				this.exchange = companyProfile.exchange || '';
				this.finnhubIndustry = companyProfile.finnhubIndustry || '';
				this.logo = companyProfile.logo || '';
				this.marketCapitalization = companyProfile.marketCapitalization || 0;
				this.name = companyProfile.name || '';
				this.shareOutstanding = companyProfile.shareOutstanding || 0;
				this.weburl = companyProfile.weburl || '';

				this.loading = false;
				resolve();
			});
		});
	}

	fetchHistoryPrices() {
		return new Promise((resolve, reject) => {

			if (this.ticker.length === 0) {
				reject('Ticker is empty');
			}

			this.loading = true;
			
			let date = new Date();
			let fiveYearsAgo = Math.round(date.getTime() / 1000) - 60 * 60 * 24 * 365 * 5;
			let now = Math.round(date.getTime() / 1000);

			finnhubClient.stockCandles(this.ticker, 'D', fiveYearsAgo, now, {}, (e, data, r) => {
				
				if (e !== null) {
					this.loading = false;
					reject({ error: e, response: r });
				}

				let prices = [];
				for (let i = 0; i < data.t.length; i++) {
					prices.push({
						x: new Date(data.t[i] * 1000),
						y: [data.o[i], data.h[i], data.l[i], data.c[i]],
					});
				}

				this.prices = prices;

				this.loading = false;
				resolve();
			});
		});
	}

}

/**
 * Stock collection
 */
export class Stocks {

	models = [];

	constructor(stocks = []) {

		if (!Array.isArray(stocks)) {
			throw new Error('Not array.');
		}

		this.models = stocks.map(stock => new Stock(stock));
	}

	isEmpty() {
		return this.models.length === 0;
	}

	findByTicker(ticker) {
		return this.models.find(stock => stock.ticker === ticker);
	}

	/**
	 * @param {boolean} checkInCollection Check if collection has this value already.
	 * @param {Stock} stock Stock model
	 * @returns {Stock} Stock model
	 */
	add(stock, checkInCollection = true) {
		
		if (checkInCollection) {
			const inCollection = this.findByTicker(stock.ticker);

			if (inCollection) {
				return inCollection;
			}
		}

		this.models.push(stock);

		return stock;
	}

	sort() {
		console.log('sort');
	}

	remove() {
		console.log('remove');
	}

}